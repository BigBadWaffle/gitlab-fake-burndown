// ==UserScript==
// @name         GitLab Fake Burndown
// @namespace    http://tampermonkey.net/
// @version      1.0
// @description  Shows a burndown chart for a GitLab milestone when pressing Escape
// @author       @bigbadwofl
// @match        YOUR URL GOES HERE
// @grant        none
// @require      https://code.jquery.com/jquery-3.4.1.slim.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js
// ==/UserScript==

const msPerDay = 24 * 60 * 60 * 1000;
const containerPadding = 100;

const colorRequired = '255, 99, 132';
const colorActual = '99, 132, 255';

const containerCss = {
	position: 'fixed',
	left: 0,
	top: 0,
	width: '100vw',
	height: '100vh',
	display: 'flex',
	justifyContent: 'center',
	alignItems: 'center',
	zIndex: 99999,
	padding: containerPadding,
	backgroundColor: 'rgba(255, 255, 255, 0.95)',
	visibility: 'hidden'
};

const getMonthName = monthNum => {
	return [
		'Jan',
		'Feb',
		'Mar',
		'Apr',
		'May',
		'Jun',
		'Jul',
		'Aug',
		'Sep',
		'Oct',
		'Nov',
		'Dec'
	][monthNum];
};

$(() => {
	let show = false;

	const valStart = $('.start_date .value-content span').html();
	const dateStart = new Date(valStart);

	const valEnd = $('.due_date .value-content span').html();
	const dateEnd = new Date(valEnd);

	const dateNow = new Date();

	const daysTotal = (dateEnd - dateStart) / msPerDay;
	const daysPassed = ~~((dateNow - dateStart) / msPerDay);

	const totalIssues = $('.issuable-row').length;
	const totalDone = $('#issues-list-closed .issuable-row').length;
	const actualDonePerDay = totalDone / daysPassed;
	const requiredDonePerDay = totalIssues / daysTotal;

	const labels = [];
	const dataActual = [];
	const dataRequired = [];

	for (let i = 0; i <= daysTotal; i++) {
		const dateThis = new Date(valStart);
		dateThis.setDate(dateThis.getDate() + i);
		labels.push(`${getMonthName(dateThis.getMonth())} ${dateThis.getDate()}`);

		dataRequired.push(requiredDonePerDay * i);
		if (i < daysPassed)
			dataActual.push(actualDonePerDay * i);
		else
			dataActual.push(actualDonePerDay * daysPassed);
	}

	const el = $('<div><canvas></div>')
		.appendTo('body')
		.css(containerCss);

	const canvas = el.find('canvas')[0];
	const ctx = canvas.getContext('2d');

	let dataConfig = {
		labels,
		lineAtIndex: ~~daysPassed,
		datasets: [{
			label: 'Actual',
			backgroundColor: `rgba(${colorActual}, 0.2)`,
			borderColor: `rgba(${colorActual}, 1)`,
			pointRadius: 0,
			data: dataActual
		}, {
			label: 'Required',
			backgroundColor: `rgba(${colorRequired}, 0.2)`,
			borderColor: `rgba(${colorRequired}, 1)`,
			pointRadius: 0,
			data: dataRequired
		}]
	};

	let options = {
		scales: {
			yAxes: [{
				gridLines: {
					display: true,
					color: 'rgba(90, 90, 90, 0.2)'
				}
			}],
			xAxes: [{
				gridLines: {
					display: false
				}
			}]
		}
	};

	let originalLineDraw = Chart.controllers.line.prototype.draw;
	Chart.helpers.extend(Chart.controllers.line.prototype, {
		draw: function () {
			originalLineDraw.apply(this, arguments);

			let chart = this.chart;
			let ctx = chart.chart.ctx;

			let index = chart.config.data.lineAtIndex;
			if (index) {
				let xaxis = chart.scales['x-axis-0'];
				let yaxis = chart.scales['y-axis-0'];

				ctx.save();
				ctx.beginPath();
				ctx.moveTo(xaxis.getPixelForValue(undefined, index), yaxis.top);
				ctx.strokeStyle = '#999';
				ctx.lineTo(xaxis.getPixelForValue(undefined, index), yaxis.bottom);
				ctx.stroke();
				ctx.restore();
			}
		}
	});

	Chart.Line(ctx, {
		options: options,
		data: dataConfig
	});

	$('body').on('keydown', e => {
		if (e.originalEvent.key !== 'Escape')
			return;

		show = !show;
		if (show)
			el.css({ visibility: 'visible' });
		else
			el.css({ visibility: 'hidden' });
	});
});
